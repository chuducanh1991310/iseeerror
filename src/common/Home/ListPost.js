import React, { Component } from 'react'
import {
    View, StyleSheet, ScrollView,TouchableOpacity
} from 'react-native'
import { Title, Heading ,Text} from '@shoutem/ui';

//import
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import colors from '../../config/colors';
import ItemPost from '../ItemPost';
import { WIDTH, HEIGHT } from '../../config/Function';

type Props = {
    title: string,
    onButton: Function,
}
export default ListPost = (props: any) => {
    const { data, onButton,onViewAll, title,index } = props;
    return (
        <View style={{
            width: WIDTH(360),
            paddingHorizontal: WIDTH(10),
            marginTop: HEIGHT(20)
        }}>
            <View style={{
                 width: WIDTH(340),
                 paddingHorizontal: WIDTH(10),
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems:'flex-end',
            }}>
                <Title
                    style={{
                        width:WIDTH(220),
                        flexWrap:'wrap'
                    }}
                    styleName="bold"
                    numberOfLines={2}
                >{title}</Title>
                <TouchableOpacity
                    onPress={()=>onViewAll(index)}
                    style={{flex:0}}
                >
                    <Text style={styles.text}>Xem tất cả ></Text>
                </TouchableOpacity>
            </View>

            <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                style={styles.container}>
                <View style={{
                    flex: 0,
                    marginTop: HEIGHT(10),
                    justifyContent: 'center',
                    alignItems: 'center',
                    flexDirection: 'row'
                }}>
                    {
                        data.map((value, index) => {
                            return (
                                <ItemPost
                                     key={index}
                                     content={value}
                                     onButton={() => onButton(value)}
                                />
                            )
                        })
                    }
                </View>
            </ScrollView>
        </View>

    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    text: {
        color: colors.red,
        fontSize: STYLES.fontSizeText,
    },
})

