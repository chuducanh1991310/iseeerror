import React, { Component } from 'react'
import {
    View, StyleSheet, ScrollView, TouchableOpacity
} from 'react-native'
//import
import STYLES from '../../config/styles.config';
import colors from '../../config/colors';
import { WIDTH, HEIGHT, calListImage } from '../../config/Function';
import ItemImage from '../ItemImage';

type Props = {
    title: string,
    onButton: Function,
}
export default ListImage = (props) => {
    const { data, onButton } = props;
    let listLeft=[];
    let listRight=[];
    data.map((value, index) => {
        let listImage=calListImage(value);
        if (index % 2==0)
            listLeft.push(
                <ItemImage
                    key={index}
                    marginTop={WIDTH(10)}
                    widthImage={WIDTH(165)}
                    content={listImage[0]}
                    onButton={() => onButton(value)}
                    index={index}
                />
            )
        else 
            listRight.push(
                <ItemImage
                    key={index}
                    marginTop={WIDTH(10)}
                    widthImage={WIDTH(165)}
                    content={listImage[0]}
                    onButton={() => onButton(value)}
                    index={index}
                />
            )})
    return (
        <ScrollView
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            style={{
                flex: 1,
            }}>
            <View style={{
                width: WIDTH(360),
                flexDirection: 'row',
                justifyContent:'center',
                paddingHorizontal: WIDTH(10),
            }}>
                <View style={{
                    flex:0,
                    flexDirection:'column',
                    marginRight:WIDTH(10),
                    width: WIDTH(165),
                }}>
                    {listLeft}
                </View>
                <View style={{
                    flex:0,
                    flexDirection:'column',
                    width: WIDTH(165),
                }}>
                    {listRight}
                </View>
            </View>

        </ScrollView>

    )
}
const styles = StyleSheet.create({
    container: {
        minHeight: HEIGHT(200),
        width: WIDTH(360),
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        color: colors.red,
        fontSize: STYLES.fontSizeText,
    },
})

