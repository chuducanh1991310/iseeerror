import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image,TouchableOpacity
} from 'react-native'
import { SearchBar } from 'react-native-elements';
import Entypo from 'react-native-vector-icons/Entypo';
//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import colors from '../config/colors';
import { WIDTH, HEIGHT } from '../config/Function';

type Props = {
  title:string,
  onButton:Function,
}
export default ChooseDate = (props: any) => {
    const {title,onButton,content,onMaps}= props;  
    return (
        <View style={styles.cntMain}>
        <SearchBar
            placeholder={title}
            containerStyle={{
                backgroundColor: colors.white, 
                width: WIDTH(300),
                borderTopWidth:0,
                borderBottomWidth:0
            }}
            inputContainerStyle={{ elevation:2,backgroundColor: '#ffff', borderRadius: WIDTH(20) }}
            inputStyle={{  fontSize: STYLES.fontSizeNormalText, fontFamily: 'Roboto' }}
            onChangeText={onButton}
            value={content}
        />
        <TouchableOpacity
            style={{
                height:WIDTH(30),
                width:WIDTH(30),
                justifyContent:'center',
                alignItems:'center'
            }}
            onPress={()=>onMaps()}
        >
            <Entypo name={'location'} size={WIDTH(26)} color="#000" />
        </TouchableOpacity>
        
    </View>
    )
}
const styles = StyleSheet.create({
      cntMain: {
        width:WIDTH(360),
        marginTop:HEIGHT(20),
        paddingHorizontal: WIDTH(10),
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
      },
      text: {
        color: colors.colorWhite,
        fontSize: STYLES.fontSizeText,
        textAlign: "center",
        textAlignVertical: "center",
        fontFamily: 'Roboto-Regular',
      },
})

