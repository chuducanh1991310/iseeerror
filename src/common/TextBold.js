import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image,
} from 'react-native'

//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import colors from '../config/colors';

type Props = {
    color:string,
    content:string,
    fontSize:string,
}
export default TextBold = (props: any) => {
    const {color,fontSize,content,width}= props;  
    return (
        <Text style={[styles.text,{
            color,fontSize
        },width!=undefined ? {width} :{}]}>{content}</Text>
    )
}
const styles = StyleSheet.create({
      text: {
        color: colors.colorBlack,
        fontSize: STYLES.fontSizeText,
        flexWrap: 'wrap',
        fontWeight:'500',
        fontFamily: 'Roboto-Regular',
      },
})

