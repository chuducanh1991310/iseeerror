import { Dimensions } from "react-native";
import STYLES from "./styles.config";
import colors from "./colors";
import IMAGE from '../assets/image';
const { width, height } = Dimensions.get('window');
export const responsiveHeight = (h) => {
	return height * (h / 100);
};
export const responsiveWidth = (w) => {
	return width * (w / 100);
};
export const calListImage = (Place) => {
	let listImage = Place.listImage.split(';');
	return listImage;
};

export const calPinColor = (type) => {
	if (type==2) return colors.yellowCalendar;
	if (type==6) return colors.blueCalendar;
	if (type==4) return colors.colorRed;
	if (type==5) return colors.orangeNew;
	if (type==3) return colors.greenCalendar;
	return colors.yellowCalendar;
};
export const calimagePin = (type) => {
	if (type==2) return IMAGE.pinCaPhe;
	if (type==6) return IMAGE.pinKhac;
	if (type==4) return IMAGE.pinHomeStay;
	if (type==5) return IMAGE.pinRapCP;
	if (type==3) return IMAGE.pinTraSua;
	return IMAGE.pinCaPhe;
};

export const WIDTH = (x) => {
	return x * STYLES.widthScreen / 360;
}
export const HEIGHT = (x) => {
	return x * STYLES.heightScreen / 640;
}
export const checkRegister = (username, password, phoneNumber) => {
	if (username == '') return "Tài khoản không được để trống";
	if (password == '') return "Mật khẩu không được để trống";
	if (phoneNumber == '' || phoneNumber.length <= 8 || phoneNumber.length >= 12) return "Sai định dạng số điện thoại";
	return true;
}
export const calCoordinates = (value) => {
	if (value==undefined) return {
		latitude: 0,
		longitude: 0
	}
	let arr = value.split(',');
	let marker = {
		latitude: Number(arr[0]),
		longitude: Number(arr[1])
	};
	return marker;
}

export const calTime = (timeStart, timeEnd) => {
	let minusStart = timeStart.getTime();
	let minusEnd = timeEnd.getTime();
	let res = (minusEnd - minusStart) / 1000;
	return Math.floor(res);
};
export const responsiveWidthComponent = (widthComponent, w) => {
	return widthComponent * (w / 100);
};

export const responsiveFontSize = (f) => {
	return Math.sqrt((height * height) + (width * width)) * (f / 100);
};

export const responsiveVar = (h, v) => {
	return v * (h / 100);
};

export const slitNumber = (str) => {
	let res = "";
	for (let i = 0; i < str.length; i++)
		if (str[i] != '.') res = res + " " + str[i];
	return res;
};