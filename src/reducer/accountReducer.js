import { DATA_TOPIC } from "../config/default";

const defaultState = {
	selectedTopic: DATA_TOPIC,
	account: {},
};
export default (state = defaultState, action) => {
	switch (action.type) {
		case 'CHANGE_TOPIC': {
			let selectedTopic = state.selectedTopic;
			selectedTopic[action.id].selected = !selectedTopic[action.id].selected;
			return {
				...state,
				selectedTopic
			};
		}
		case 'CHANGE_ACCOUNT': return {
			...state,
			account: action.content,
		};
		default:
			break;
	}
	return state;
};