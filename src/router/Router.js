//import lib
import {  createStackNavigator,createAppContainer } from 'react-navigation';
import React from 'react';

import Details from '../container/Details/Details';
import Home from '../container/Home/Home';
import SelectTopic from '../container/SelectTopic/SelectTopic';
import ViewAll from '../container/ViewAll/ViewAll';
import SignIn from '../container/SignIn/SignIn';
import SignUp from '../container/SignUp/SignUp';
import Intro from '../container/Intro/Intro';
import ScreenMaps from '../container/Maps/ScreenMaps';
import Profile from '../container/Profile/Profile';
import TabView from '../container/TabView/TabView';
import PlaceAutoComplete from '../container/Maps/PlaceAutoComplete'

console.disableYellowBox = true
const AppNavigator = createStackNavigator({
	Details: { screen: Details },
	PlaceAutoComplete: { screen: PlaceAutoComplete},
	SelectTopic: { screen: SelectTopic },
	ViewAll: { screen: ViewAll },
	SignIn: { screen: SignIn },
	SignUp: { screen: SignUp },
	ScreenMaps: { screen: ScreenMaps },
	Profile: { screen: Profile },
	Intro: { screen: Intro },
	TabView: { screen: TabView },
	Home: { screen: Home },
	
}, {
		initialRouteName: 'ScreenMaps',
		// swipeEnabled: true,
		animationEnabled: false,
		headerMode: 'none',
		navigationOptions: {
			header: null
		},
		lazy: true,
		cardStyle: {
			backgroundColor: '#FFF',
			opacity: 1
		},
	})
const Router=createAppContainer(AppNavigator);
export default Router;
