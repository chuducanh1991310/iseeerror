import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image, TouchableOpacity, ImageBackground,
  TextInput, Keyboard, TouchableWithoutFeedback, Alert, AsyncStorage
} from 'react-native';
import { connect } from 'react-redux';
//import
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import colors from '../../config/colors';
import { FetchJson, REGISTER_SERVICE } from '../../servers/serivce.config';
import { WIDTH, HEIGHT, checkRegister } from '../../config/Function';
import { INIT_STORGE } from '../../config/default';
import { changeAccount } from '../../actions/actionCreators';
import { LoadingComponent } from '../../common/LoadingComponet';

type Props = {
  title: string,
}
class SignUp extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      username: '',
      password: '',
      phoneNumber: '',
      heightImage: 90,
    }
  }
  componentWillMount() {
    this.keyboardWillShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardWillShow);
    this.keyboardWillShowListener = Keyboard.addListener('keyboardDidHide', this._keyboardWillHide);
  }
  componentWillUnmount() {
    this.keyboardWillShowListener.remove();
    this.keyboardWillShowListener.remove();
  }
  _keyboardWillShow = (event) => {
    this.setState({
      heightImage: 45
    });
  };

  _keyboardWillHide = (event) => {
    this.setState({
      heightImage: 90
    });

  };
  skip() {
    const { dispatch } = this.props.navigation;
    dispatch({
      type: 'Navigation/RESET',
      actions: [{
        type: 'Navigate',
        routeName: 'Home'
      }], index: 0
    });
  }
  onButtonAlert = (notif) => {
    Alert.alert(
      'Thông báo',
      notif,
      [
        { text: 'OK' }
      ],
      { cancelable: false }
    )
  }
  onButton() {
    const { dispatch } = this.props.navigation;
    const { username, password, phoneNumber } = this.state;
    var account = {
      username: username,
      password: password,
      hoVaTen: phoneNumber
    }
    let notif = checkRegister(username, password, phoneNumber);
    this.setState({ isLoading: true })
    if (notif == true) {
      FetchJson(REGISTER_SERVICE, account).then((response: any) => {
        this.setState({ isLoading: false })
        if (response.hasOwnProperty('status') && response.status) {
          AsyncStorage.multiSet([
            [INIT_STORGE, JSON.stringify(response.rows[0])],
          ]);
          this.props.changeAccount(response.rows[0]);
          dispatch({
            type: 'Navigation/RESET',
            actions: [{
              type: 'Navigate',
              routeName: 'SelectTopic'
            }], index: 0
          });
        } else {
          this.onButtonAlert('Đăng kí không thành công.')
        }
      })
    } else this.onButtonAlert(notif);

  }
  render() {
    return (
      <TouchableWithoutFeedback
        style={{ flex: 1 }} onPress={() => Keyboard.dismiss()}>
        <ImageBackground
          style={{
            width: STYLES.widthScreen,
            height: STYLES.heightScreen,
            alignItems: 'center',
            justifyContent: 'center',
          }}
          source={IMAGE.isee6}
        >
          <Text style={{
            fontSize: this.state.heightImage,
            fontFamily: 'Playlist-Script',
            color: 'white',
          }}>
            I See
        </Text>
          <View style={[styles.frameSignIn]}>
            <View style={styles.viewUser}>
              <TextInput
                style={styles.textEmail}
                placeholder='Tài khoản'
                autoCapitalize='none'
                placeholderTextColor={"#ffff"}
                returnKeyType='next'
                underlineColorAndroid={'transparent'}
                onChangeText={(username) => this.setState({ username })}
                value={this.state.username}
              />
            </View>
            <View style={styles.viewUser}>
              <TextInput
                style={styles.textEmail}
                placeholder='Mật khẩu'
                autoCapitalize='none'
                placeholderTextColor={"#ffff"}
                returnKeyType='next'
                secureTextEntry={true}
                underlineColorAndroid={'transparent'}
                onChangeText={(password) => this.setState({ password })}
                value={this.state.password}
              />
            </View>
            <View style={styles.viewUser}>
              <TextInput
                style={styles.textEmail}
                placeholder='Số điện thoại'
                keyboardType={'numeric'}
                placeholderTextColor={"#ffff"}
                returnKeyType='next'
                underlineColorAndroid={'transparent'}
                onChangeText={(phoneNumber) => this.setState({ phoneNumber })}
                value={this.state.phoneNumber}
              />
            </View>
          </View>
          <View style={styles.viewButton}>
            <TouchableOpacity
              style={styles.button}
              onPress={() => this.onButton()}
            >
              <Text style={{ fontSize: STYLES.fontSizeNormalText, color: 'black' }}>
                ĐĂNG KÝ
                  </Text>
            </TouchableOpacity>
          </View>
          <LoadingComponent
            isLoading={this.state.isLoading}
          />
        </ImageBackground>
      </TouchableWithoutFeedback>
    )
  }
}

function mapStateToProps(state) {
  return {
    account: state.accountReducer.account,
  };
}

export default connect(mapStateToProps, {
  changeAccount
})(SignUp);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textNonal: {
    color: 'white',
    fontSize: STYLES.fontSizeText,
  },
  text: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: STYLES.fontSizeText,
  },
  image: {
    width: 120 * STYLES.widthScreen / 360,
    height: 80 * STYLES.heightScreen / 640,
  },
  viewOfimage: {
    width: STYLES.widthScreen,
    height: 313 * STYLES.heightScreen / 640,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.colorPink,
    shadowColor: '#E5E5E5',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 3,
    shadowRadius: 2,
    elevation: 2,
  },
  frameSignIn: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 0,
    marginBottom: 35 * STYLES.heightScreen / 640,
  },
  viewButton: {
    width: 250 * STYLES.widthScreen / 360,
    height: 52 * STYLES.heightScreen / 640,
    shadowColor: '#8B8989',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 3,
    shadowRadius: 2,
    elevation: 5,
  },
  button: {
    width: 250 * STYLES.widthScreen / 360,
    height: 40 * STYLES.heightScreen / 640,
    backgroundColor: colors.white,
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewUser: {
    width: 250 * STYLES.widthScreen / 360,
    height: 60 * STYLES.heightScreen / 640,
    borderBottomWidth: 1.5,
    borderColor: '#ffff',
    justifyContent: 'center',
    marginBottom: 5 * STYLES.heightScreen / 640,
  },
  textEmail: {
    color: 'white',
    fontSize: STYLES.fontSizeText,
    width: 250 * STYLES.widthScreen / 360,
    height: 40 * STYLES.heightScreen / 640,
  },
  viewSkip: {
    flex: 0,
    marginTop: HEIGHT(10),
  }
})

