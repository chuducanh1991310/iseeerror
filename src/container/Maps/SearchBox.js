import React from 'react'
import { Text } from 'react-native';
import { View, InputGroup, Input } from 'native-base'
import Icon from 'react-native-vector-icons/FontAwesome'
import styles from '../Details/styles';
export const SearchBox = () => {
    return (
        <View style={styles.searchBox}>
            <View style={styles.inputWrapper}>
                <Text>Chon</Text>
                <InputGroup>
                    <Icon name="search" size={15} color="red" />
                    <GooglePlacesAutocomplete
          placeholder="Search"
          minLength={2} // minimum length of text to search
          autoFocus={false}
          returnKeyType={"search"}
          listViewDisplayed="false"
          fetchDetails={true}
          renderDescription={row =>
            row.description || row.formatted_address || row.name
          }
          onPress={(data, details = null) => {}}
          getDefaultValue={() => {
            return ""; // text input default value
          }}
          query={{
            key: "*********",
            language: "en", // language of the results
            types: "(cities)" // default: 'geocode'
          }}
          styles={{
            description: {
              fontWeight: "bold"
            },
            predefinedPlacesDescription: {
              color: "#1faadb"
            }
          }}
          enablePoweredByContainer={true}
          nearbyPlacesAPI="GoogleReverseGeocoding"
          GooglePlacesSearchQuery={{
            rankby: "distance",
            types: "food"
          }}
          filterReverseGeocodingByTypes={[
            "locality",
            "administrative_area_level_3"
          ]}
          debounce={200}
        />
                </InputGroup>
            </View>
        </View>
    )
}
export default SearchBox;
