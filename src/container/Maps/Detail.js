import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Dimensions,Modal,
    ScrollView, Linking,TouchableWithoutFeedback
} from 'react-native'
const { height, width } = Dimensions.get('window');
import ImageViewer from 'react-native-image-zoom-viewer';
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import Entypo from 'react-native-vector-icons/Entypo';
import { Title, Caption, Lightbox, Image } from '@shoutem/ui';
import styles from './styles';
import { WIDTH, HEIGHT,calListImage } from '../../config/Function';

export default class Detail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isModal: false,
            idImage:0,
        }
    }
    onTouchable = (val,id) => {
        this.setState({ isModal: val,idImage:id })
    }
    render() {
        const { Place, navigation } = this.props;
        let listImage = Place.listImage.split(';');
        let images=[];
        listImage.map((value)=>images.push({url:value}));
        return (
            <View style={styles.containerStyle}>
                <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    style={{
                        flex: 0,
                    }}>
                    <View style={{ flex: 0, flexDirection: "row" }}>
                        {
                            listImage.map((Place,index) => {
                                let widthImage = 180 / 360 * width;
                                let heightImage = 100 * height / 640;
                                return (
                                    <TouchableWithoutFeedback onPress={() =>this.onTouchable(true,index)}>
                                        <View style={{ flex: 0, marginLeft: 3 / 360 * width }}>
                                                <Image
                                                    source={{ uri: Place }}
                                                    style={{
                                                        width: widthImage, height: heightImage
                                                    }} />
                                        </View>
                                    </TouchableWithoutFeedback>
                                )
                            })
                        }
                    </View>
                </ScrollView>
                <Modal
                    onRequestClose={() => this.onTouchable(false,0)}
                    visible={this.state.isModal} transparent={true}>
                    <ImageViewer
                        index={this.state.idImage}
                        onSwipeDown={() => {
                            this.onTouchable(false,0)
                        }}
                        enableSwipeDown={true}
                        imageUrls={images} />
                </Modal>
                <View style={{ flex: 0, width: WIDTH(280), marginVertical: HEIGHT(5) }}>
                    <Title style={{ width: WIDTH(250) }}>{Place.namePlace}</Title>
                    <Caption numberOfLines={2}>Địa chỉ: {Place.address} </Caption>
                    <Caption>Số điện thoại: {Place.phoneNumber}</Caption>
                    <Caption numberOfLines={2}>Mô tả: {Place.describe}</Caption>
                </View>
                <View style={{ width: 320 / 360 * width, flexDirection: 'row', marginBottom: HEIGHT(10) }}>
                    <TouchableOpacity
                        onPress={() => {
                            let res = 'google.navigation:q=' + Place.coordinates;
                            Linking.openURL(res);
                        }
                        }
                        style={{ height: 30 / 640 * height, width: 112 / 360 * width, backgroundColor: '#1A73E8', borderRadius: 13, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', elevation: 2 }}>
                        <FontAwesome name="share-square" color="#fff" size={WIDTH(20)} />
                        <Text style={{ color: '#fff', marginLeft: 10 / 360 * width }}>Chỉ đường</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => navigation.navigate('Details', {
                            content: Place
                        })}
                        style={{ height: 30 / 640 * height, width: 112 / 360 * width, backgroundColor: '#fff', borderRadius: 13, flexDirection: 'row', marginLeft: 10, borderWidth: 1, borderColor: '#1A73E8', justifyContent: 'center', alignItems: 'center', elevation: 2 }}>
                        <Entypo name="paper-plane" color="#1A73E8" size={WIDTH(20)} />
                        <Text style={{ color: '#1A73E8', marginLeft: 10 / 360 * width }}>Chi tiết</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

}