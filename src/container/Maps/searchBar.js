import React, { Component } from 'react';
import { TouchableHightLight, Text, View, FlatList, TouchableOpacity } from 'react-native';
import { SearchBar } from 'react-native-elements'
import Icon from 'react-native-vector-icons/MaterialIcons'


const rowHeight = 40;

var self;

export default class SearchButton extends Component {

  constructor(props) {
    super(props);
    //setting default state
    this.state = {
        textString: "",
    }
  }
  
  chanegText = async (text) => {
    await this.setState({
      textString: text,
    })
    console.log(text.length);
    if (text.length == 0)
    {
      console.log('dang xoa')
      {this.props.hideList()}
    }
    this.props.SearchFilterFunction(text);
  }

  clearBar = () => {
    console.log("hello");
    {this.props.hideList()}
  }

  render() {
    // inside your render function
    return (
      <View style={{ flex: 1 }}>
        <SearchBar
          platform = "android"
          onClear = {() => this.clearBar()}
          placeholder="Type Here..."
          onChangeText={(text) => { this.chanegText(text) }}
          value={this.state.textString}
          lightTheme={true}
          containerStyle={{
            borderRadius: 10,
          }}
          inputStyle={{
            height: 10,
          }}
        />
      </View>
    );
  }
}