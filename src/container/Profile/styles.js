import {
    StyleSheet,
    Dimensions
} from 'react-native'

import STYLES from '../../config/styles.config'
const { height, width } = Dimensions.get('window');
export default styles = StyleSheet.create({
    ctnStyle:{
        flex:0,
        width:360/360*width,
        //marginLeft:17/360*width
    },
    headerStyle:{
        paddingLeft: 20*width/360,
        paddingBottom:20*height/640,
        paddingTop:20*height/640,
        borderBottomColor:'#494949',
        borderBottomWidth:1,
    },
    textSetting:{
        fontSize:STYLES.fontSizeLarge,
        color:'#1C1C1C'
    },
    bodyStyle:{
        flex:0,
        paddingBottom:30*height/640,
        paddingTop:40/630*height,
        alignItems:'center',
        // borderTopColor:'black',
        // borderTopWidth:1
         borderBottomColor:'#ddd',
         borderBottomWidth:1
    },
    viewImage:{
        height:162/640*height,
        width:162/640*height,
        justifyContent:"center",
        alignItems:'center',
    },
    viewImage:{
        height:162/640*height,
        width:162/640*height,
        backgroundColor:'#fff',
        borderColor:"#98F5FF",
        borderRadius:81/640*height,
        justifyContent:"center",
        alignItems:'center',
        borderWidth:3,
    },
    imageStyle:{
        height:152/640*height,
        width:152/640*height,
        borderRadius:76/640*height,
      // resizeMode:'contain'
    },
    viewPen:{
        borderTopLeftRadius:35/640*height,
        borderTopRightRadius:35/640*height,
        height:35/640*height,
        width:70*height/640,
        backgroundColor:'#fff',
        position:'absolute',
        bottom:0,
        paddingBottom:2,
        left:46/640*height,
        alignItems:"center",
        justifyContent:'flex-end'
    },
    viewAcount:{
        padding:5,
        // borderWidth:1,
        // borderColor:'red',
        alignItems:'center'
    },
    nameAcount:{
        paddingTop:5/640*height,
        paddingBottom:5/640*height,
        flex:3,
        alignItems:'center',
        justifyContent:'center'
    },
    levelMember:{
        paddingBottom:10/640*height
    },
    cntChoice:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        borderBottomWidth:1,
        borderBottomColor:'#ddd',
        paddingTop:10*height/640,
        paddingBottom:10*height/640,
        paddingRight:10*height/640,
    },
    textChoice:{
        color:'#1C1C1C',
        fontSize:STYLES.fontSizeNormalText
    },
    Textname:{
        fontSize:STYLES.fontSizeLabel,
        color:'#1C1C1C',
        
    },
    text:{
        color:'#1C1C1C',
        fontSize:STYLES.fontSizeNormalText
    }
    

})