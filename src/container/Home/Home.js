/**
 * This component to show account
 * @huanhtm
 */
import React, { Component } from 'react'
import {
    View, StatusBar,Image
} from 'react-native'

import { connect } from 'react-redux';
import styles from './styles'

//import common

import { LIST_DATA_TOPIC, LIST_CA_PHE_MAPS, LIST_CAFE, LIST_TRA_SUA, LIST_HOME_STAY, LIST_RAP_CP, LIST_HOT, TITLE_HOME, LIST_ALL } from '../../config/default';
import { incCount, decCount } from '../../actions/actionCreators'

import colors from '../../config/colors';
import ItemSearch from '../../common/ItemSearch';
import IMAGE from '../../assets/image'
import { LoadingComponent } from '../../common/LoadingComponet';

import TabView from '../TabView/TabView';

class Home extends Component<Props> {
    state = {
        search: '',
        idTitle: 0,
        isLoading: true
    };
    componentWillMount() {
        clearTimeout(this.interval);
        this.interval = setTimeout(() => {
            this.setState({ isLoading: false })
        }, 1000);
    }
    updateSearch = search => {
        this.setState({ search });
    };
    onPost = (content) => {
        this.props.navigation.navigate('Details', { content: content })
    }
    onViewAll = (id) => {
        if (id == 1) this.props.navigation.navigate('ViewAll', {
            content: {
                title: 'Quán cà phê',
                data: LIST_CA_PHE_MAPS,
            }
        })
        if (id == 2) this.props.navigation.navigate('ViewAll', {
            content: {
                title: 'Quán trà sữa',
                data: LIST_TRA_SUA,
            }
        })
        if (id == 3) this.props.navigation.navigate('ViewAll', {
            content: {
                title: 'Homestay',
                data: LIST_HOME_STAY,
            }
        })
        if (id == 4) this.props.navigation.navigate('ViewAll', {
            content: {
                title: 'Rạp chiếu phim mini',
                data: LIST_RAP_CP,
            }
        })
        if (id == 5) this.props.navigation.navigate('ViewAll', {
            content: {
                title: 'Địa điểm khác',
                data: LIST_HOT,
            }
        })
    }

    render() {
        const { search } = this.state;
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor={colors.lightGrayEEE} />
                <ItemSearch
                    title={'Tìm kiếm địa điểm...'}
                    content={search}
                    onMaps={()=>this.props.navigation.navigate("ScreenMaps")}
                    onButton={this.updateSearch}
                />
                    <TabView
                        screenProps={this.props.navigation}
                    />
                <LoadingComponent
                    isLoading={this.state.isLoading}
                />
            </View>
        )
    }
}
function mapStateToProps(state) {
    return {
        count: state.rootReducer.count,
        account: state.accountReducer.account,
    };
}

export default connect(mapStateToProps, {
    incCount, decCount
})(Home);


{/* <ScrollView
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    style={{
                        flex: 1,
                    }}>
                    <View style={styles.cntMain}>
                        <ListSelfie
                            data={LIST_HOT}
                            onButton={this.onPost}
                        />
                        {
                            LIST_DATA_TOPIC.map((value, index) => {
                                return (
                                    <ListPost
                                        key={index}
                                        index={index}
                                        title={value.title}
                                        data={value.data}
                                        onButton={this.onPost}
                                        onViewAll={(id)=> this.onViewAll(id)}
                                    />
                                )
                            })
                        }

                    </View>
                </ScrollView> */}